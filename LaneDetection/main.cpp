#include<opencv\cv.h>
#include<opencv\highgui.h>
#include<opencv2\opencv.hpp>
#include<opencv2\core\core.hpp>
#include<opencv2\imgproc\imgproc.hpp>

using namespace cv;
using namespace std;

Mat origImg, grayImg, detectedEdges, canny, topImg, hough;
int edgeThresh = 1;
int lowerThreshold;
int ratio = 3;
int kernel_size = 3;
const int max_lowerThreshold = 100;
char Name[10] = "Edge Map";
char * windowName = Name;
Point2f src_vertices[4];
Point2f dst_vertices[4];



void CannyThreshold(int, void*) {
	blur(grayImg, detectedEdges, Size(3, 3));
	//Canny(detectedEdges, detectedEdges, lowerThreshold, lowerThreshold*ratio, kernel_size);
	try {
		Canny(detectedEdges, detectedEdges, 180, 250, kernel_size);
		//Canny(detectedEdges, detectedEdges, lowerThreshold, lowerThreshold*ratio, kernel_size);
	}
	catch (Exception e) {
		cout << endl << e.msg << endl;
	}
	canny = Scalar::all(0);
	grayImg.copyTo(canny, detectedEdges);
	namedWindow(windowName, CV_WINDOW_AUTOSIZE);
	imshow(windowName, canny);
	waitKey(0);
}

void houghTrans() {
	vector<Vec2f> lines;
	HoughLines(canny, lines, 1, CV_PI / 180, 150, 0, 0);
	for (size_t i = 0; i < lines.size(); i++)
	{
		float rho = lines[i][0], theta = lines[i][1];
		Point pt1, pt2;
		double a = cos(theta), b = sin(theta);
		double x0 = a * rho, y0 = b * rho;
		pt1.x = cvRound(x0 + 1000 * (-b));
		pt1.y = cvRound(y0 + 1000 * (a));
		pt2.x = cvRound(x0 - 1000 * (-b));
		pt2.y = cvRound(y0 - 1000 * (a));
		line(hough, pt1, pt2, Scalar(0, 0, 255), 3, CV_AA);
	}
}

//void BirdView(Point2f* src_vertices, Point2f* dst_vertices, Mat& src, Mat &dst) {
//	Mat M = getPerspectiveTransform(src_vertices, dst_vertices);
//	warpPerspective(origImg, dst, M, dst.size(), INTER_LINEAR, BORDER_CONSTANT);
//}

void printImg() {
	namedWindow("origImg");
	imshow("origImg", origImg);
	waitKey(0);
	namedWindow("dst");
	imshow("dst", canny);
	waitKey(0);
	/*namedWindow("dst2");
	imshow("dst2", dst2);
	waitKey(0);*/
	system("pause");
}

void initBird() {
	/*src_vertices[0] = Point(390, 410);
	src_vertices[1] = Point(530, 410);
	src_vertices[2] = Point(780, 510);
	src_vertices[3] = Point(170, 510);*/

	src_vertices[0] = Point(365, 180);
	src_vertices[1] = Point(440, 180);
	src_vertices[2] = Point(790, 350);
	src_vertices[3] = Point(30, 350);

	/*dst_vertices[0] = Point(0, 0);
	dst_vertices[1] = Point(960, 0);
	dst_vertices[2] = Point(960, 650);
	dst_vertices[3] = Point(0, 650);*/

	dst_vertices[0] = Point(0, 0);
	dst_vertices[1] = Point(790, 0);
	dst_vertices[2] = Point(790, 370);
	dst_vertices[3] = Point(0, 370);

	Mat M = getPerspectiveTransform(src_vertices, dst_vertices);
	canny = (790, 370, 16);// CV_8UC3); 650, 960, 16
	topImg = (790, 370, 16);// CV_8UC3);
	warpPerspective(origImg, topImg, M, canny.size(), INTER_LINEAR, BORDER_CONSTANT);

	namedWindow("topview");
	imshow("topview", topImg);
}


int main()
{	
	grayImg = imread("G:\\Dokumente\\OpenCVRoot\\LaneDetection\\LaneDetection\\Road_desert.jpg",CV_LOAD_IMAGE_GRAYSCALE);
	origImg = imread("G:\\Dokumente\\OpenCVRoot\\LaneDetection\\LaneDetection\\Road_desert.jpg", CV_LOAD_IMAGE_COLOR);

	initBird();	
	//BirdView(src_vertices, dst_vertices, grayImg, topImg);
	//printImg();
	

	/*createTrackbar("Min. Threshold: ", windowName, &lowerThreshold, max_lowerThreshold, CannyThreshold);
	CannyThreshold(0, 0);
	cvtColor(canny, hough, CV_GRAY2BGR);
	houghTrans();
	namedWindow("hough");
	imshow("hough", hough);*/

	//createTrackbar("Min. Threshold: ", windowName, &lowerThreshold, max_lowerThreshold, CannyThreshold);
	CannyThreshold(0, 0);
	try {
		cvtColor(canny, hough, CV_GRAY2BGR);
	}
	catch (Exception e) {
		cout << endl << e.msg << endl;
	}
	houghTrans();
	namedWindow("hough");
	imshow("hough", hough);


	waitKey(0);

	/*imwrite("canny.jpg", canny);
	imwrite("detectedLines_1.jpg", hough);
	imwrite("birdview_desert_gray.jpg", topImg);
	imwrite("gray_desert.jpg", grayImg);*/

	system("pause");
}

